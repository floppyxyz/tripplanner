import React, {useEffect} from 'react';
import classes from './Contentbox.module.css';
import Searchfield from "./components/searchfield";
import {Card, Typography} from "@mui/material";
import StopContainer from "./components/StopContainer";


export interface Stop {
    placesResult: google.maps.places.PlaceResult;
    children?: Stop[];
}

const ContentOverlay = () => {

    const [tripStops, setTripStops] = React.useState<Stop[]>(() => {
        const stickyValue = window.localStorage.getItem("tripStops");
        return stickyValue !== null
            ? JSON.parse(stickyValue)
            : [];
    });

    // @ts-ignore
    const [title, setTitle] = React.useState("Japan 2023");

    useEffect(() => {
        localStorage.setItem("tripStops", JSON.stringify(tripStops));
    }, [tripStops]);

    const addStop = (stop: google.maps.places.PlaceResult) => {

        setTripStops([...tripStops, {placesResult: stop}]);
    }

    const removeStop = (stop: Stop) => {
        setTripStops(tripStops.filter((s) => s !== stop));
    }

    return (
        <Card className={classes.contentbox}>
            <Typography variant="h4" component="h2" gutterBottom> {title} </Typography>

                <StopContainer stops={tripStops} removeStop={removeStop}/>

            <Searchfield addStop={addStop} placeholder={"Neues Hauptziel hinzufügen"}/>
        </Card>
    );
};

export default ContentOverlay;