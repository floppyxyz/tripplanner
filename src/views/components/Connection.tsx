import React from 'react';
import Searchfield from "./searchfield";

const Connection = (props: {break?: boolean, breakTitle?: string, children?: React.ReactElement}) => {
    return (
        <div>
            {props.break ? `|-> ${props.breakTitle}` : "|"}
            {props.children}
        </div>
    );
};

export default Connection;