import React from 'react';
import classes from "./StopContainer.module.css";
import MainStop from "./MainStop";
import {Stop} from "../ContentOverlay";

const StopContainer = (props: {stops: Stop[], removeStop: (stop:Stop) => void}) => {
    return (
        <div className={classes.stopContainer}>
            {
                props.stops.map((stop, index) => {
                    return (
                        <div id={stop.placesResult.place_id}>
                        <MainStop stop={stop} removeStop={props.removeStop}/>
                            {/*{ index !== props.stops.length -1 && <Connection/>}*/}
                        </div>
                    );
                })
            }
        </div>
    );
};

export default StopContainer;