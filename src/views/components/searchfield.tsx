import React from 'react';

import { StandaloneSearchBox} from "@react-google-maps/api";
import classes from "./searchfield.module.css";

interface Props {
    addStop: (stop: google.maps.places.PlaceResult) => void;
    placeholder: string;
    parentStop?: google.maps.places.PlaceResult;
}

const Searchfield = (props: Props) => {

        const [searchboxRef, setSearchboxRef] = React.useState<google.maps.places.SearchBox | null>(null);

        const options = {bounds: undefined};
        if(props.parentStop && props.parentStop?.geometry?.viewport) {
            // @ts-ignore
            options.bounds = props.parentStop?.geometry?.viewport;
        }

        const onLoad = (ref: any) => setSearchboxRef(ref);
        const onPlacesChanged = () => {
                if(searchboxRef && searchboxRef?.getPlaces()?.length !== 0) {
                        // @ts-ignore
                        props.addStop(searchboxRef?.getPlaces()[0]);
                }
        }
        return (
            <div className={classes.container}>
                <StandaloneSearchBox
                    onLoad={onLoad}
                    onPlacesChanged={onPlacesChanged}
                    options={options}
                >
                    <
                        input
                        type="text"
                        className={classes.searchfield}
                        placeholder={props.placeholder}
                        style={
                            {
                                boxSizing: `border-box`,
                                border: `1px solid transparent`,
                                width: `240px`,
                            height
                            :
                            `32px`,
                            padding
                            :
                            `0 12px`,
                            borderRadius
                            :
                            `3px`,
                            boxShadow
                            :
                            `0 2px 6px rgba(0, 0, 0, 0.3)`,
                            fontSize
                            :
                            `14px`,
                            outline
                            :
                            `none`,
                            textOverflow
                            :
                            `ellipses`,
                            position
                            :
                            "absolute",
                            left
                            :
                            "50%",
                            marginLeft
                            :
                            "-120px"
                        }
                        }
                    />
                </StandaloneSearchBox>
            </div>
        )
            ;
    }
;

export default Searchfield;