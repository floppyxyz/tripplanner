import React from 'react';
import {Stop} from "../ContentOverlay";
import classes from './MainStop.module.css';
import {Button, Typography} from "@mui/material";
import Searchfield from "./searchfield";
import HotelIcon from '@mui/icons-material/Hotel';
import DirectionsBusIcon from '@mui/icons-material/DirectionsBus';
import FlightTakeoffIcon from '@mui/icons-material/FlightTakeoff';
import CarRentalIcon from '@mui/icons-material/CarRental';


const MainStop = (props: { stop: Stop , removeStop: (stop:Stop) => void}) => {

    return (
        <div >
            <div className={classes.title}>
                <div className={classes.titleRow}>
                    <Typography variant={"h4"}>{props.stop.placesResult.name}</Typography>
                    <Button variant={"contained"} color={"warning"} onClick={() => {props.removeStop(props.stop)}}>X</Button>
                </div>
                <div>
                    {props.stop.placesResult.website}
                </div>
            </div>
            <div className={classes.mainStopBox}>
                <div className={classes.affiliateBox}>
                    <Button variant="contained" color="primary" onClick={() => {}}><HotelIcon/></Button>
                    <Button variant="contained" color="secondary" onClick={() => {}}><DirectionsBusIcon/></Button>
                    <Button variant="contained" color="secondary" onClick={() => {}}><CarRentalIcon/></Button>
                    <Button variant="contained" color="secondary" onClick={() => {}}><FlightTakeoffIcon/></Button>
                </div>
                <div className={classes.stopBox}>
                    {props.stop.placesResult.html_attributions}
                </div>
                {/*<Searchfield addStop={() => {}} placeholder={"Zwischenziel in " + props.stop.placesResult.name + " hinzufügen"}/>*/}
            </div>
        </div>
    );
};

export default MainStop;