import React from 'react';
import logo from './logo.svg';
import './App.css';
import Map from "./views/Map";
import ContentOverlay from "./views/ContentOverlay";
import {LoadScript} from "@react-google-maps/api";

function App() {
  return (
    <div className="App">
        <LoadScript googleMapsApiKey={"AIzaSyAs3Gtfe8A-RbtrC6IYGOcf8nSSTkTOkKw"} libraries={["places"]}>
        <Map />
        <ContentOverlay />
        </LoadScript>
    </div>
  );
}

export default App;
